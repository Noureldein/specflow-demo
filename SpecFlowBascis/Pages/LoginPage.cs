﻿using OpenQA.Selenium;
using SpecFlowBascis.Drivers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace SpecFlowBascis.Pages
{
    public class LoginPage
    {
        private readonly IDriveFixture _driveFixture;
        public LoginPage(IDriveFixture driveFixture)
        {
            _driveFixture = driveFixture;
        }

        //UI elements
        public IWebElement lnkLogin => _driveFixture.driver.FindElement(By.LinkText("Login"));
        public IWebElement txtUsername => _driveFixture.driver.FindElement(By.Id("Username"));
        public IWebElement txtPassowrd => _driveFixture.driver.FindElement(By.Id("Password"));
        public IWebElement btnSubmit => _driveFixture.driver.FindElement(By.ClassName("btn-primary"));

        public void clickLogin() => lnkLogin.Click();
        public void EnterData(string username, object password)
        {
            txtUsername.SendKeys(username);
            txtPassowrd.SendKeys(password.ToString());
        }
        public void clickSubmit() => btnSubmit.Click();
        public void ShowALert()
        {
            _driveFixture.driver.SwitchTo().Alert().Accept();
            Thread.Sleep(3000);

        }

    }
}
