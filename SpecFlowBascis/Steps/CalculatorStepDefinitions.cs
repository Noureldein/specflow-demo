﻿using System;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SpecFlowBascis.Steps
{
    [Binding]
    public sealed class CalculatorStepDefinitions
    {

        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        private readonly ScenarioContext _scenarioContext;

        public CalculatorStepDefinitions(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given("the first number is (.*)")]
        public void GivenTheFirstNumberIs(int number)
        {
            Console.WriteLine($"{ nameof(GivenTheFirstNumberIs)}:{number}");

        }

        [Given("the second number is (.*)")]
        public void GivenTheSecondNumberIs(int number)
        {
            Console.WriteLine($"second number: {number}");
        }

        [When(@"the two numbers are added")]
        public void WhenTheTwoNumbersAreAdded()
        {
            Console.WriteLine($"total number");
        }

        [Then(@"the result should be (.*)")]
        public void ThenTheResultShouldBe(int p0)
        {
            Console.WriteLine($"total number:{p0}");

        }
        [Given(@"I input the many numbers")]
        public void GivenIInputTheManyNumbers(Table table)
        {
            //var data = table.CreateSet<Calcualtion>();            
            var data = table.CreateDynamicSet();

            foreach (var item in data)
            {
                Console.WriteLine($"the number:{item.Numbers}");
            }
        }

        [When(@"the two numbers added")]
        public void WhenTheTwoNumbersAdded()
        {
            Console.WriteLine($"{ nameof(WhenTheTwoNumbersAdded)}");
        }

        [Then(@"the result should ""(.*)""")]
        public void ThenTheResultShould(string p0)
        {
            Console.WriteLine($"{ nameof(ThenTheResultShould)}");
        }

        [Then(@"I see the result few  more details")]
        public void ThenISeeTheResultFewMoreDetails(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            Console.WriteLine($"the result is:{data.Results} And the Logo is {data.Logo}");
        }



    }

    public record Calcualtion
    {
        public int Numbers { get; set; }
    }
}
