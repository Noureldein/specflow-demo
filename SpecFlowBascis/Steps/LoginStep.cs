﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using SpecFlowBascis.Drivers;
using SpecFlowBascis.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SpecFlowBascis.Steps
{
    [Binding]
    public sealed class LoginStep
    {
        private readonly IDriveFixture _driveFixture;
        private readonly LoginPage loginPage;
        public LoginStep(IDriveFixture driveFixture)
        {
            _driveFixture = driveFixture;
            loginPage = new LoginPage(driveFixture);
        }

        [Given(@"I lanch Application")]
        public void GivenILanchApplication()
        {
            //_driveFixture.driver.Manage().Window.Maximize();
            _driveFixture.driver.Navigate().GoToUrl("https://localhost:44328/");
        }

        [Given(@"I click login navbar")]
        public void GivenIClickLoginNavbar()
        {
            loginPage.clickLogin();
        }

        [Given(@"I enter following data")]
        public void GivenIEnterFollowingData(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            loginPage.EnterData((string)data.Username, data.Password);
        }

        [Given(@"I click submit")]
        public void GivenIClickSubmit()
        {
            loginPage.clickSubmit();
        }

        [Then(@"I should see all the details modules in navbar")]
        public void ThenIShouldSeeAllTheDetailsModulesInNavbar()
        {
            //IJavaScriptExecutor js = _driveFixture.driver as IJavaScriptExecutor;
            //js.ExecuteScript($"alert( login successfull)");

            //IAlert simpleAlert = _driveFixture.driver.SwitchTo().Alert();
            //String alertText = simpleAlert.Text;
            //simpleAlert.Accept();
            //// '.Text' is used to get the text from the Alert
            //loginPage.ShowALert();
            Console.WriteLine(nameof(ThenIShouldSeeAllTheDetailsModulesInNavbar));
        }

        [Then(@"I should not see all the details modules in navbar")]
        public void ThenIShouldNotSeeAllTheDetailsModulesInNavbar()
        {
            Console.WriteLine(nameof(ThenIShouldNotSeeAllTheDetailsModulesInNavbar));
        }


    }
}
