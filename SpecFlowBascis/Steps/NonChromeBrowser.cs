﻿using OpenQA.Selenium;
using SpecFlowBascis.Drivers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;
using TechTalk.SpecFlow.Assist;

namespace SpecFlowBascis.Steps
{
    [Binding]
    public sealed class NonChromeBrowser
    {
        // For additional details on SpecFlow step definitions see https://go.specflow.org/doc-stepdef

        //private IWebDriver driver;
        private readonly ScenarioContext _scenarioContext;

        public NonChromeBrowser(ScenarioContext scenarioContext)
        {
            _scenarioContext = scenarioContext;
        }

        [Given(@"I navigate to App on following browesrs")]
        public void GivenINavigateToAppOnFollowingBrowesrs(Table table)
        {
            dynamic data = table.CreateDynamicInstance();
            IWebDriver driver1 = _scenarioContext.Get<SeleniumDriver>().Setup("Chrome");
            //Login Address
            driver1.Url = "https://localhost:44328/";
        }


        //[Given(@"click to login anchor")]
        //public void GivenClickToLoginAnchor()
        //{
        //    driver.FindElement(By.LinkText("Login")).Click();
        //}


        //[Given(@"Enter the username item")]
        //public void GivenEnterTheUsernameItem()
        //{
        //    driver.FindElement(By.Name("Username")).SendKeys("momkenAdmin");
        //}

        //[Given(@"Enter the Password item")]
        //public void GivenEnterThePasswordItem()
        //{
        //    driver.FindElement(By.Name("Password")).SendKeys("25802580");
        //}

        //[When(@"click submit Login")]
        //public void WhenClickSubmitLogin()
        //{
        //    driver.FindElement(By.ClassName("btn btn-primary")).Click();
        //}


    }
}
