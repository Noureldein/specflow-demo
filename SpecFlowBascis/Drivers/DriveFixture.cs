﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SpecFlowBascis.Drivers
{
    public interface IDriveFixture
    {
        public IWebDriver driver { get; }
    }
    public class DriveFixture : IDriveFixture
    {
        private readonly IWebDriver _webDriver;
        public DriveFixture()
        {
            _webDriver = GetWebDriver();
            _webDriver.Manage().Window.Maximize();
        }

        private IWebDriver GetWebDriver() => new ChromeDriver();

        public IWebDriver driver => _webDriver;
    }
}
