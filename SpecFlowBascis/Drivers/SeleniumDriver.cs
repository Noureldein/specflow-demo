﻿using OpenQA.Selenium;
using OpenQA.Selenium.Chrome;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Remote;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TechTalk.SpecFlow;

namespace SpecFlowBascis.Drivers
{
    public class SeleniumDriver
    {
        //private IWebDriver driver;
        private readonly ScenarioContext _scenairoContext;
        public SeleniumDriver(ScenarioContext scenairoContext) => _scenairoContext = scenairoContext;

        public IWebDriver Setup(string browserName)
        {
            dynamic capacity = GetBrowserOption(browserName);
            // local address
            IWebDriver driver = new RemoteWebDriver(new Uri("https://localhost:44328/"), new ChromeOptions().ToCapabilities());

            // Set the driver
            _scenairoContext.Set(driver, "WebDriver");
            driver.Manage().Window.Maximize();
            return driver;
        }

        private dynamic GetBrowserOption(string BrowserName)
        {
            switch (BrowserName.ToLower())
            {
                case "chrome":
                    return new ChromeOptions();
                case "firefox":
                    return new FirefoxOptions();
                default:
                    return new ChromeOptions();
            }
        }
    }
}
