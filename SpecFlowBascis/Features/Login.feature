﻿Feature: Login
	Login to Admin Dashboard

@smoke
Scenario: Perform Login of Admin Login
	#steps 
	Given I lanch Application
	And I click login navbar
	And I enter following data
		| Username   | Password |
		| momknAdmin | 25802580 |
	And I click submit 
	Then I should see all the details modules in navbar

	@smoke1
Scenario: Perform Login of Admin Login with wrong data
	#steps 
	Given I lanch Application
	And I click login navbar
	And I enter following data
		| Username   | Password |
		| momknAdmin | password |
	And I click submit 
	Then I should not see all the details modules in navbar