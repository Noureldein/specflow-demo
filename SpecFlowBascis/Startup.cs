﻿using Microsoft.Extensions.DependencyInjection;
using SolidToken.SpecFlow.DependencyInjection;
using SpecFlowBascis.Drivers;
using System;
using System.Threading.Tasks;


namespace SpecFlowBascis
{
    public class Startup
    {
        [ScenarioDependencies]
        public static IServiceCollection CreateServices()
        {
            var services = new ServiceCollection();
            services.AddScoped<IDriveFixture, DriveFixture>();
            return services;
        }

    }
}
