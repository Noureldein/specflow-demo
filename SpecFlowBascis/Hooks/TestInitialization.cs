﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using TechTalk.SpecFlow;


namespace SpecFlowBascis.Hooks
{
    [Binding]
    public sealed class TestInitialization
    {
        // For additional details on SpecFlow hooks see http://go.specflow.org/doc-hooks

        [BeforeScenario("mytag")]
        public static void BeforeScenario()
        {
            Console.WriteLine("this line executed BEFORE scenario");
        }

        [BeforeTestRun]
        public static void BeforeTestRun()
        {
            Console.WriteLine("this line executed  BEFORE TestRun");
        }

        [AfterFeature]
        public static void BeforeFeature()
        {
            Console.WriteLine("this line executed  Before Feature");
        }

        [AfterScenario]
        public static void AfterScenario()
        {
            Console.WriteLine("this line executed AFTER scenario");
        }
    }
}
